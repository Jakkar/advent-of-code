use std::{
    fs::File,
    io::{BufRead, BufReader},
};
fn main() {
    let reader = BufReader::new(File::open("./day6/input.txt").expect("Unable to find file"));

    // eeeh
    let data: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    let fish: Vec<i32> = data[0].split(',').map(|num| num.parse().unwrap()).collect();

    part1(fish.clone(), 80);
    part2(fish, 256);
}

fn part1(mut fish_arr: Vec<i32>, num_days: i32) {
    for _ in 0..num_days {
        for j in 0..fish_arr.len() {
            fish_arr[j] -= 1;
            if fish_arr[j] == -1 {
                fish_arr[j] = 6;
                fish_arr.push(8);
            }
        }
    }

    println!("Part1: {}", fish_arr.len());
}

fn part2(fish_arr: Vec<i32>, num_days: i32) {
    let mut sorted_fish: [usize; 10] = [0; 10];

    fish_arr.into_iter().for_each(|fish| {
        sorted_fish[fish as usize + 1] += 1;
    });

    for _ in 0..num_days {
        for i in 1..10 {
            sorted_fish[i - 1] = sorted_fish[i];
        }

        sorted_fish[9] = sorted_fish[0];
        sorted_fish[7] += sorted_fish[0];
        sorted_fish[0] = 0;
    }

    println!("Part2: {}", sorted_fish.iter().sum::<usize>());
}
