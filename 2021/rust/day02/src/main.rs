use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let reader = BufReader::new(File::open("./day2/input.txt").expect("Unable to find file"));

    // eeeh
    let instructions: Vec<String> = reader
        .lines()
        .map(|l| l.expect("Unable to parse line"))
        .collect();

    part1(&instructions);
    part2(&instructions);
}

fn part1(instructions: &[String]) {
    let mut depth = 0;
    let mut position = 0;

    instructions
        .iter()
        .map(|instruction| instruction.split_whitespace().collect::<Vec<&str>>())
        .for_each(|instruction| match instruction[0] {
            "forward" => position += instruction[1].parse::<i32>().expect("Not a number :O"),
            "down" => depth += instruction[1].parse::<i32>().expect("Not a number :O"),
            "up" => depth -= instruction[1].parse::<i32>().expect("Not a number :O"),
            _ => println!("{} is an invalid instruction!", instruction[0]),
        });

    println!("Part1: {}", depth * position);
}

fn part2(instructions: &[String]) {
    let mut depth = 0;
    let mut position = 0;
    let mut aim = 0;

    instructions
        .iter()
        .map(|instruction| instruction.split_whitespace().collect::<Vec<&str>>())
        .for_each(|instruction| match instruction[0] {
            "forward" => {
                position += instruction[1].parse::<i32>().expect("Not a number :O");
                depth += aim * instruction[1].parse::<i32>().expect("Not a number :O");
            }
            "down" => aim += instruction[1].parse::<i32>().expect("Not a number :O"),
            "up" => aim -= instruction[1].parse::<i32>().expect("Not a number :O"),
            _ => println!("{} is an invalid instruction!", instruction[0]),
        });

    println!("Part2: {}", depth * position);
}
