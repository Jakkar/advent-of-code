use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let reader = BufReader::new(File::open("./day5/input.txt").expect("Unable to find file"));

    // eeeh
    let lines: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    let lines: Vec<Vec<i32>> = lines
        .iter()
        .map(|line| {
            line.replace(" -> ", ",")
                .split(',')
                .map(|num| num.parse().unwrap())
                .collect()
        })
        .collect();

    part1(lines.clone());
    part2(lines);
}

// part2 is much better go look at part 2 instead it uses cool algorithm and stuff
fn part1(lines: Vec<Vec<i32>>) {
    let mut map: [[i32; 1000]; 1000] = [[0; 1000]; 1000];

    lines.into_iter().for_each(|coords| {
        if coords[0] == coords[2] {
            let (y_big, y_small): (i32, i32);

            if coords[1] > coords[3] {
                y_big = coords[1];
                y_small = coords[3];
            } else {
                y_big = coords[3];
                y_small = coords[1];
            }

            for i in y_small..=y_big {
                map[coords[0] as usize][i as usize] += 1;
            }
        } else if coords[1] == coords[3] {
            let (x_big, x_small): (i32, i32);

            if coords[0] > coords[2] {
                x_big = coords[0];
                x_small = coords[2];
            } else {
                x_big = coords[2];
                x_small = coords[0];
            }

            for i in x_small..=x_big {
                map[i as usize][coords[1] as usize] += 1;
            }
        }
    });

    println!(
        "Part1: {}",
        map.iter().flatten().filter(|val| **val > 1).count()
    );
}

fn part2(lines: Vec<Vec<i32>>) {
    let mut map: [[i32; 1000]; 1000] = [[0; 1000]; 1000];

    lines
        .iter()
        .for_each(|points| plot_line(&mut map, points[0], points[1], points[2], points[3]));

    println!(
        "Part2: {}",
        map.iter().flatten().filter(|val| **val > 1).count()
    );
}

// Bresenham's line algorithm as described on Wikipedia https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm

fn plot_line(map: &mut [[i32; 1000]; 1000], x_0: i32, y_0: i32, x_1: i32, y_1: i32) {
    if (y_1 - y_0).abs() < (x_1 - x_0).abs() {
        if x_0 > x_1 {
            plot_line_low(map, x_1, y_1, x_0, y_0);
        } else {
            plot_line_low(map, x_0, y_0, x_1, y_1);
        }
    } else if y_0 > y_1 {
        plot_line_high(map, x_1, y_1, x_0, y_0);
    } else {
        plot_line_high(map, x_0, y_0, x_1, y_1);
    }
}

fn plot_line_low(map: &mut [[i32; 1000]; 1000], x_0: i32, y_0: i32, x_1: i32, y_1: i32) {
    let dx = x_1 - x_0;
    let mut dy = y_1 - y_0;
    let mut yi = 1;

    if dy < 0 {
        yi = -1;
        dy = -dy
    }

    let mut d = (2 * dy) - dx;
    let mut y = y_0;

    for x in x_0..=x_1 {
        map[x as usize][y as usize] += 1;

        if d > 0 {
            y += yi;
            d += 2 * (dy - dx);
        } else {
            d += 2 * dy;
        }
    }
}

fn plot_line_high(map: &mut [[i32; 1000]; 1000], x_0: i32, y_0: i32, x_1: i32, y_1: i32) {
    let mut dx = x_1 - x_0;
    let dy = y_1 - y_0;
    let mut xi = 1;

    if dx < 0 {
        xi = -1;
        dx = -dx;
    }

    let mut d = (2 * dx) - dy;
    let mut x = x_0;

    for y in y_0..=y_1 {
        map[x as usize][y as usize] += 1;

        if d > 0 {
            x += xi;
            d += 2 * (dx - dy);
        } else {
            d += 2 * dx;
        }
    }
}
