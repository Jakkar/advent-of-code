use std::{
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Clone)]
struct Board {
    // storing both rows and cols not great but makes it a little easier later :-)
    rows: Vec<Vec<i32>>,
    cols: Vec<Vec<i32>>,
    winning: bool,
    nums_to_win: i32,
}

fn main() {
    let reader = BufReader::new(File::open("./day4/input.txt").expect("Unable to find file"));

    // eeeh
    let bingo_data: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    let (winning_numbers, boards) = parse_bingo_data(bingo_data);

    part1(&winning_numbers, &boards);
    part2(&winning_numbers, &boards);
}

fn part1(winning_numbers: &[i32], boards: &[Board]) {
    let mut checked_boards: Vec<Board> = boards
        .iter()
        .map(|board| check_board(winning_numbers, board.clone()))
        .collect();

    checked_boards.sort_by(|a, b| a.nums_to_win.cmp(&b.nums_to_win));

    println!(
        "Part1: {}",
        winning_numbers[(checked_boards[0].nums_to_win - 1) as usize]
            * sum_board(winning_numbers, &checked_boards[0])
    )
}

// same as part 1 but just sorted the other way
fn part2(winning_numbers: &[i32], boards: &[Board]) {
    let mut checked_boards: Vec<Board> = boards
        .iter()
        .map(|board| check_board(winning_numbers, board.clone()))
        .filter(|board| board.winning)
        .collect();

    checked_boards.sort_by(|a, b| b.nums_to_win.cmp(&a.nums_to_win));

    println!(
        "Part2: {}",
        winning_numbers[(checked_boards[0].nums_to_win - 1) as usize]
            * sum_board(winning_numbers, &checked_boards[0])
    )
}

fn sum_board(winning_numbers: &[i32], board: &Board) -> i32 {
    board
        .rows
        .iter()
        .flatten()
        .filter(|num| {
            !winning_numbers
                .iter()
                .take(board.nums_to_win as usize)
                .any(|&x| x == **num) // funny business
        })
        .sum()
}

fn parse_bingo_data(bingo_data: Vec<String>) -> (Vec<i32>, Vec<Board>) {
    // vector of winning numbers
    let winning_numbers = bingo_data[0]
        .split(',')
        .map(|num| num.parse::<i32>().unwrap())
        .collect::<Vec<i32>>();

    // vector of boards
    let mut bingo_boards: Vec<Board> = Vec::new();

    // start at first board in lines, skip by 6 to get to start of next board
    for i in (2..bingo_data.len() - 2).step_by(6) {
        let mut current_board: Board = Board {
            rows: Vec::new(),
            cols: Vec::new(),
            winning: false,
            nums_to_win: 0,
        };

        // get the next 5 lines from current i location in lines
        bingo_data.iter().skip(i).take(5).for_each(|row| {
            // get row and add to current board
            current_board.rows.push(
                row.split_whitespace()
                    .map(|num| num.parse::<i32>().unwrap())
                    .collect::<Vec<i32>>(),
            )
        });

        // now also find the cols using the existing rows
        for i in 0..5 {
            current_board
                .cols
                .push(current_board.rows.iter().map(|row| row[i]).collect())
        }

        bingo_boards.push(current_board);
    }

    (winning_numbers, bingo_boards)
}

// please look away and spare your eyes :)
fn check_board(numbers: &[i32], mut board: Board) -> Board {
    let mut current_numbers = Vec::<i32>::new();

    for i in numbers {
        current_numbers.push(*i);

        // check all rows
        for row in board.rows.iter() {
            if row
                .iter()
                .filter(|num| current_numbers.contains(num))
                .count()
                == 5
            {
                // found win
                board.winning = true;
                board.nums_to_win = current_numbers.len() as i32;
                return board;
            }
        }

        // check all cols
        for col in board.cols.iter() {
            if col
                .iter()
                .filter(|num| current_numbers.contains(num))
                .count()
                == 5
            {
                // found win
                board.winning = true;
                board.nums_to_win = current_numbers.len() as i32;
                return board;
            }
        }
    }

    board
}
