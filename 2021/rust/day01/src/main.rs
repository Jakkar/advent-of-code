use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let reader = BufReader::new(File::open("./day1/input.txt").expect("Unable to find file"));

    // eeeh
    let numbers: Vec<i32> = reader
        .lines()
        .map(|l| {
            l.expect("Unable to parse line")
                .parse()
                .expect("Not a number :O")
        })
        .collect();

    part1(&numbers);
    part2(&numbers);
}

fn part1(numbers: &[i32]) {
    let increases = numbers.windows(2).filter(|pair| pair[1] > pair[0]).count();

    println!("Part 1: {}", increases);
}

fn part2(numbers: &[i32]) {
    let sums: Vec<i32> = numbers.windows(3).map(|tuple| tuple.iter().sum()).collect();
    let increases = sums.windows(2).filter(|pair| pair[1] > pair[0]).count();

    println!("Part 2: {}", increases)
}
