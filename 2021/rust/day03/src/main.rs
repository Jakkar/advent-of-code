use std::{
    cmp::Ordering,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let reader = BufReader::new(File::open("./day3/input.txt").expect("Unable to find file"));

    // eeeh
    let consumption: Vec<String> = reader
        .lines()
        .map(|l| l.expect("Unable to parse line"))
        .collect();

    part1(&consumption);
    part2(&consumption);
}

fn part1(consumption: &[String]) {
    let gamma = get_most_common_bits(consumption, '0', false);

    // invert gamma for epsilon
    let epsilon = gamma
        .chars()
        .into_iter()
        .map(|bit| if bit == '1' { '0' } else { '1' })
        .collect::<String>();

    // convert to numbers
    let epsilon = usize::from_str_radix(&epsilon, 2).unwrap();
    let gamma = usize::from_str_radix(&gamma, 2).unwrap();

    // :)
    println!("Part1: {}", gamma * epsilon);
}

fn part2(consumption: &[String]) {
    let mut values_o2 = consumption.to_vec();
    let mut values_co2 = values_o2.clone();

    // find most common
    for i in 0..consumption[0].chars().count() {
        if values_o2.len() == 1 {
            break;
        }

        let bits: Vec<char> = get_most_common_bits(&values_o2, '1', false)
            .chars()
            .collect();

        values_o2 = values_o2
            .into_iter()
            .filter(|value| value.chars().nth(i).unwrap() == bits[i])
            .collect();
    }

    // find least common
    for i in 0..consumption[0].chars().count() {
        if values_co2.len() == 1 {
            break;
        }

        let bits: Vec<char> = get_most_common_bits(&values_co2, '0', true)
            .chars()
            .collect();

        values_co2 = values_co2
            .into_iter()
            .filter(|value| value.chars().nth(i).unwrap() == bits[i])
            .collect();
    }

    let value_o2 = usize::from_str_radix(&values_o2[0], 2).unwrap();
    let value_co2 = usize::from_str_radix(&values_co2[0], 2).unwrap();

    println!("Part2: {}", value_o2 * value_co2);
}

// "get most common" is kinda not correct cuz "reverse" will make it find least common but it is what it is
fn get_most_common_bits(data: &[String], eq_decider: char, reversed: bool) -> String {
    // just assuming all lines are same length
    let length = data[0].chars().count();

    let mut output = String::new();

    // check cols
    for i in 0..length {
        let sum: u32 = data
            .iter()
            .map(|value| value.chars().nth(i).unwrap().to_digit(10).unwrap())
            .sum();

        // funky floaty matchy ify reversy chain woa
        match (sum as f32)
            .to_bits()
            .cmp(&(data.len() as f32 / 2.0).to_bits())
        {
            Ordering::Equal => output.push(eq_decider),
            Ordering::Less => {
                if reversed {
                    output.push('1')
                } else {
                    output.push('0')
                }
            }
            Ordering::Greater => {
                if reversed {
                    output.push('0')
                } else {
                    output.push('1')
                }
            }
        }
    }

    output
}
