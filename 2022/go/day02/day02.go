package main

import (
	_ "embed"
	"fmt"
	"strings"
)

//go:embed input.txt
var input string

var points = map[string]int{
	"X": 1,
	"Y": 2,
	"Z": 3,
}

var plays = map[string]map[string]int{
	"A": {
		"Z": points["Y"] + 6, // winning
		"X": points["Z"],     // losing
		"Y": points["X"] + 3, // drawing
	},
	"B": {
		"Z": points["Z"] + 6, // winning
		"X": points["X"],     // losing
		"Y": points["Y"] + 3, // drawing
	},
	"C": {
		"Z": points["X"] + 6, // winning
		"X": points["Y"],     // losing
		"Y": points["Z"] + 3, // drawing
	},
}

func main() {
	input = strings.TrimRight(input, "\n")
	var input = strings.Split(input, "\n")

	part1(input)
	part2(input)
}

func part1(input []string) {
	var total int

	for _, hands := range input {
		var hands = strings.Split(hands, " ")
		total += rps_part1(hands[0], hands[1])
	}

	fmt.Println(total)
}

func part2(input []string) {
	var total int

	for _, round := range input {
		var round = strings.Split(round, " ")
		total += plays[round[0]][round[1]]
	}

	fmt.Println(total)
}

func rps_part1(opponent string, player string) int {
	var score = points[player]

	// hot
	switch opponent {
	case "A":
		if player == "Y" {
			score += 6
		}

		if player == "X" {
			score += 3
		}
	case "B":
		if player == "Z" {
			score += 6
		}

		if player == "Y" {
			score += 3
		}
	case "C":
		if player == "X" {
			score += 6
		}

		if player == "Z" {
			score += 3
		}
	}

	return score
}
