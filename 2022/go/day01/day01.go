package main

import (
	"aoc/utils"
	_ "embed"
	"fmt"
	"sort"
	"strings"
)

//go:embed input.txt
var input string

func main() {
	input = strings.TrimRight(input, "\n")
	var input = strings.Split(input, "\n\n")

	part1(input)
	part2(input)
}

func part1(input []string) {
	highest := 0

	for _, elf := range input {
		total := 0
		for _, val := range strings.Split(elf, "\n") {
			total += utils.ToInt(val)
		}

		if total > highest {
			highest = total
		}
	}

	fmt.Println(highest)
}

func part2(input []string) {
	var elves []int

	for _, elf := range input {
		total := 0

		for _, val := range strings.Split(elf, "\n") {
			total += utils.ToInt(val)
		}

		elves = append(elves, total)
	}

	sort.Sort(sort.Reverse(sort.IntSlice(elves)))

	fmt.Println(elves[0] + elves[1] + elves[2])
}
